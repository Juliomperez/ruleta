package com.example.ruleta

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    lateinit var etEmail:EditText
    lateinit var etPassword:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etEmail=findViewById(R.id.etEmail)
        etPassword=findViewById(R.id.etPassword)
    }

    fun btnLogin(v:View){
        var textoUser:String=etEmail.text.toString()
        var textoPassword:String=etPassword.text.toString()

        if (textoUser=="user@ucjc.edu" && textoPassword=="123456"){
            var intent: Intent = Intent(baseContext,RouletteActivity::class.java)
            startActivity(intent)
            this.finish()
        }

    }

    fun btnSingin(v:View){
        var intent: Intent = Intent(baseContext,SecondActivity::class.java)
        startActivity(intent)
        this.finish()
    }
}